#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *create_race_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *switch_left_msg();
static cJSON *switch_right_msg();
static cJSON *turbo_msg();
static cJSON *make_msg(char *type, cJSON *msg);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

int is_anybody_on_left_lane();
int is_anybody_on_right_lane();
int is_anybody_on_same_lane();

#if 0
FILE* datafile = NULL;
FILE* accelerationFile = NULL;
FILE* brakeFile = NULL;
#endif

/* own functions */

static void debug_json( cJSON *obj) {
	//int type = obj->type;

	printf("this %p ", obj);
	printf("next %p " , obj->next);
	printf("prev %p " , obj->prev);
 	printf("child %p ", obj->child);
	printf("type %d ", obj->type); 

	printf("valuestring %s ", obj->valuestring);
	printf("valueint %d ", obj->valueint);
	printf("valuedouble %f ", obj->valuedouble);
	printf("string %s ", obj->string);
	printf("\n");
/*
	if (obj->child != NULL) {
		debug(obj->child);
	}
	if (obj->next != NULL) {
		debug(obj->next);
	}
*/
}

/* game init */

struct piece_object {
	float length;
	int is_length;
	float radius;
	int is_radius;
	float angle;
	int is_angle;
	int is_switch;
	int is_bridge;
};
int num_of_pieces;
struct piece_object piece_table[100];

static void read_pieces(cJSON *array) {

	cJSON* ptr1 = array;
	cJSON* ptr2 = NULL;
	num_of_pieces = 0;
	while (ptr1 != NULL) {
		//printf("*********************************\n");
		piece_table[num_of_pieces].is_length = 0;
		piece_table[num_of_pieces].is_angle = 0;
		piece_table[num_of_pieces].is_radius = 0;
		piece_table[num_of_pieces].length = 0;
		piece_table[num_of_pieces].angle = 0;
		piece_table[num_of_pieces].radius = 0;
		piece_table[num_of_pieces].is_switch = 0;
		piece_table[num_of_pieces].is_bridge = 0;
		ptr2 = ptr1->child;
		while (ptr2 != NULL) {

			if (ptr2->child != NULL)
				printf("KAAAAAAAAAAAAAAABBOOOOOOOOOOOOMM");
			//printf("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			if (!strcmp(ptr2->string, "length")) {
				//printf("length = %f", ptr2->valuedouble);
				piece_table[num_of_pieces].is_length = 1;
				piece_table[num_of_pieces].length = ptr2->valuedouble;
				}
			if (!strcmp(ptr2->string, "angle")) {
				//printf("angle = %f", ptr2->valuedouble);
				piece_table[num_of_pieces].is_angle = 1;
				piece_table[num_of_pieces].angle = ptr2->valuedouble;
				}
			if (!strcmp(ptr2->string, "radius")) {
				//printf("radius = %f", ptr2->valuedouble);
				piece_table[num_of_pieces].is_radius = 1;
				piece_table[num_of_pieces].radius = ptr2->valuedouble;
				}
			if (!strcmp(ptr2->string, "switch")) {
				//printf("switch = %f", ptr2->valueint);
				piece_table[num_of_pieces].is_switch = ptr2->valueint;
				}
			if (!strcmp(ptr2->string, "bridge")) {
				//printf("bridge = %f", ptr2->valueint);
				piece_table[num_of_pieces].is_bridge = ptr2->valueint;
				}
			//debug_json(ptr2);
			ptr2 = ptr2->next;
		}
		num_of_pieces++;
		ptr1 = ptr1->next;
	}

}
	

struct lane_object {
	float distanceFromCenter;
	int index;
	int is_distanceFromCenter;
	int is_index;
};
int num_of_lanes;
struct lane_object lane_table[10];

static void read_lanes(cJSON *array) {

	cJSON* ptr1 = array;
	cJSON* ptr2 = NULL;
	num_of_lanes = 0;
	while (ptr1 != NULL) {
		//printf("*********************************\n");
		lane_table[num_of_lanes].is_distanceFromCenter = 0;
		lane_table[num_of_lanes].is_index = 0;
		ptr2 = ptr1->child;
		while (ptr2 != NULL) {

			if (ptr2->child != NULL)
				printf("KAAAAAAAAAAAAAAABBOOOOOOOOOOOOMM");
			//printf("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			if (!strcmp(ptr2->string, "distanceFromCenter")) {
				//printf("length = %f", ptr2->valuedouble);
				lane_table[num_of_lanes].is_distanceFromCenter = 1;
				lane_table[num_of_lanes].distanceFromCenter = ptr2->valuedouble;
				}
			if (!strcmp(ptr2->string, "index")) {
				//printf("angle = %f", ptr2->valuedouble);
				lane_table[num_of_lanes].is_index = 1;
				lane_table[num_of_lanes].index = ptr2->valueint;
				}
			//debug_json(ptr2);
			ptr2 = ptr2->next;
		}
		num_of_lanes++;
		ptr1 = ptr1->next;
	}

}

struct gameInitInfo  {
	char id[32];
	char name[32];	
	float x;
	float y;
	float angle;
	float laps;
	float maxLapTimeMs;
	int quickRace;
	int durationMs;
};

struct gameInitInfo gameInit;

struct car_object {
	char name[32];
	char color[32];
	float length;
	float width;
	float guideFlagPosition;

	/* dynamic data */
	float angle;
	int pieceIndex;
	float inPieceDistance;
	int startLaneIndex;
	int endLaneIndex;
	int lap;

	float speed;
	float acceleration;

	/* history */
	float old_angle;
	int old_pieceIndex;
	float old_inPieceDistance;
	int old_startLaneIndex;
	int old_endLaneIndex;
	int old_lap;

	float old_speed;
	float old_acceleration;

};

struct car_object car_table[32];

int my_car_index = 0xFF;
int num_of_cars = 0;

int g_gameTick= 0;

char myName[32];
char myColor[32];

static void read_cars(cJSON* array3) {
	cJSON* driverId = NULL;
	cJSON* driverName =NULL;
	cJSON* color = NULL;
	cJSON* dimensions = NULL;
	cJSON* carLength = NULL;
	cJSON* carWidth = NULL;
	cJSON* guideFlagPosition = NULL;

	num_of_cars = 0;
	my_car_index = 0xFF;

	while(array3 != NULL) {
	driverId = array3->child;
	driverName = driverId->child;
	sprintf(car_table[num_of_cars].name,"%s",driverName->valuestring);

	//debug_json(driverName);
	if (!strcmp(driverName->valuestring,myName)) {
		my_car_index = num_of_cars;
		//printf("my car: %d\n", my_car_index);
	}
	color = driverName->next;
	sprintf(car_table[num_of_cars].color,"%s",color->valuestring);
	dimensions = driverId->next;
	carLength = dimensions->child;
	car_table[num_of_cars].length = carLength->valuedouble;
	carWidth = carLength->next;
	car_table[num_of_cars].width = carWidth->valuedouble;
	guideFlagPosition = carWidth->next;
	car_table[num_of_cars].guideFlagPosition = guideFlagPosition->valuedouble;

	car_table[num_of_cars].old_angle = 0;
	car_table[num_of_cars].old_pieceIndex = 0;
	car_table[num_of_cars].old_inPieceDistance = 0;
	car_table[num_of_cars].old_startLaneIndex = 0;
	car_table[num_of_cars].old_endLaneIndex = 0;
	car_table[num_of_cars].old_lap = 0;

	car_table[num_of_cars].old_speed = 0;
	car_table[num_of_cars].old_acceleration = 0;

	car_table[num_of_cars].angle = 0;
	car_table[num_of_cars].pieceIndex = 0;
	car_table[num_of_cars].inPieceDistance = 0;
	car_table[num_of_cars].startLaneIndex = 0;
	car_table[num_of_cars].endLaneIndex = 0;
	car_table[num_of_cars].lap = 0;

	car_table[num_of_cars].speed = 0;
	car_table[num_of_cars].acceleration = 0;

	array3 = array3->next;
	num_of_cars++;
	}
}

static void printGameInit() {
	printf("name %s\n",gameInit.name);
	printf("id %s\n",gameInit.id);
	printf("x %f\n", gameInit.x);
	printf("y %f\n", gameInit.y);
	printf("angle %f\n", gameInit.angle);
	printf("laps %f\n", gameInit.laps);
	printf("maxLapTimeMs %f\n", gameInit.maxLapTimeMs);
	printf("quickRace %d\n", gameInit.quickRace);
	printf("durationMs %d\n", gameInit.durationMs);

	int i = 0;
	for (i =0 ; i <num_of_pieces +1; i++)
		printf("len %f  %d ang %f %d rad %f %d swi %d bri %d \n" ,
			piece_table[i].length, piece_table[i].is_length,  piece_table[i].angle,  piece_table[i].is_angle,
			piece_table[i].radius, piece_table[i].is_radius,  piece_table[i].is_switch, piece_table[i].is_bridge);
	
	for (i =0 ; i <num_of_lanes +1; i++)
		printf("distance %f  %d index %d %d\n" ,
			lane_table[i].distanceFromCenter, lane_table[i].is_distanceFromCenter,  
			lane_table[i].index,  lane_table[i].is_index);

	for (i =0 ; i <num_of_cars +1; i++)
		printf("name %s color %s length %f width %f  gfp %f\n" ,
			car_table[i].name, car_table[i].color,  
			car_table[i].length, car_table[i].width,
			car_table[i].guideFlagPosition);

}

int is_qualifying = 1;
// 0.39 = germany joskus 0.43 !, 0.42 keimola, 0.41 usa
float speed_tuning = 0.43; //0.39; //0.37; //0.35;

static void handleGameInit(cJSON *obj) {
	cJSON* gameinit = NULL;
	cJSON* data = NULL;
	cJSON* race = NULL;
	cJSON* track = NULL;
	cJSON* id = NULL;
	cJSON* name = NULL;
	cJSON* pieces = NULL;
	cJSON* array1 = NULL;
	cJSON* lanes = NULL;
	cJSON* array2 = NULL;
	cJSON* startingPoint = NULL;
	cJSON* position = NULL;
	cJSON* xCoord = NULL;
	cJSON* yCoord = NULL;
	cJSON* angle = NULL;
	cJSON* cars = NULL;
	cJSON* array3 = NULL;

	cJSON* raceSession = NULL;
	cJSON* raceSessionChild = NULL;
	cJSON* laps = NULL;
	cJSON* maxLapTimeMs = NULL;
	cJSON* quickRace = NULL;
	cJSON* durationMs = NULL;
	//cJSON* ptr1 = NULL;
	//cJSON* ptr2 = NULL;

	gameinit = obj->child;
	data = gameinit->next;
	race = data->child;
	track = race->child;
	id = track->child;
	sprintf(gameInit.id,"%s",id->valuestring);
	name = id->next;
	sprintf(gameInit.name,"%s",name->valuestring);

	pieces = name->next;
	array1 = pieces->child;
	read_pieces(array1);
	lanes = pieces->next;
	array2 = lanes->child;
	read_lanes(array2);
	
	startingPoint = lanes->next;	
	position = startingPoint->child;
	xCoord = position->child;
	gameInit.x =xCoord->valuedouble;
	yCoord = xCoord->next;
	gameInit.y = yCoord->valuedouble;
	angle = position->next;
	gameInit.angle = angle->valuedouble;

	cars = track->next;
	array3 = cars->child;
	read_cars(array3);

#if 1	
	raceSession = cars->next;
	raceSessionChild = raceSession->child;
	if (!strcmp("laps", raceSessionChild->string)) {
		laps = raceSession->child;
		gameInit.laps = laps->valuedouble;
		maxLapTimeMs = laps->next;
		gameInit.maxLapTimeMs = maxLapTimeMs->valuedouble;
		quickRace = maxLapTimeMs->next;
		gameInit.quickRace = quickRace->valueint;
		is_qualifying = 0;
	}
	else if (!strcmp("durationMs", raceSessionChild->string)) {
		durationMs = raceSession->child;
		gameInit.durationMs = durationMs->valueint;
		printf("durationMs %d\n", gameInit.durationMs);
		is_qualifying = 1;
		speed_tuning = 0.43;
	}

#endif
}

static void printCarPositions() {
	int i = 0;
	for (i =0 ; i <num_of_cars; i++) {
		printf("name %s color %s length %f width %f  gfp %f tick %d\n" ,
			car_table[i].name, car_table[i].color,  
			car_table[i].length, car_table[i].width,
			car_table[i].guideFlagPosition, g_gameTick);
		printf("angle %f pieceIndex %d inPD %f, sli %d eli %d lap %d\n",
			car_table[i].angle, car_table[i].pieceIndex,
			car_table[i].inPieceDistance, car_table[i].startLaneIndex,
			car_table[i].endLaneIndex, car_table[i].lap);
		printf("old angle  %f old pieceIndex %d old_inPD %f speed %f\n",
			car_table[i].old_angle,
			car_table[i].old_pieceIndex,
			car_table[i].old_inPieceDistance,
			car_table[i].speed);
		}
}

float calculateCarSpeed(int index) {

	float length = piece_table[car_table[index].old_pieceIndex].length;
	int is_length = piece_table[car_table[index].old_pieceIndex].is_length;
	float angle = piece_table[car_table[index].old_pieceIndex].angle;
	int is_angle = piece_table[car_table[index].old_pieceIndex].is_angle;
	float radius = piece_table[car_table[index].old_pieceIndex].radius;
	int is_radius = piece_table[car_table[index].old_pieceIndex].is_radius;
	
	float dfc = lane_table[car_table[index].old_startLaneIndex].distanceFromCenter;

	float inPieceDistance = car_table[index].inPieceDistance; 
	float old_inPieceDistance = car_table[index].old_inPieceDistance;
	float speed = 0.0;
	
	speed = inPieceDistance - old_inPieceDistance;

	if ((car_table[index].pieceIndex > car_table[index].old_pieceIndex) || 
		(car_table[index].pieceIndex < car_table[index].old_pieceIndex)) {
		if (is_length) { /* straight */				
			speed+= length;
		}
		else if (((is_radius) && (is_angle)) && (angle > 0)) { /* bend to right */
			speed+= (angle * 3.14 * (radius - dfc)) / 180.0; 
		}
		else if (((is_radius) && (is_angle)) && (angle < 0)) { /* bend to left */
			speed+= ((-angle) * 3.14 * (radius  + dfc)) / 180.0; 
		}
	
#if 0
		printf("debug speed: inPd %f old_inPd %f length %f angle %f radius %f dfc %f speed %f \n", 
			inPieceDistance, old_inPieceDistance, length, angle, radius, dfc, speed);
#endif
	}

	return speed;
}

static void handleCarPositions(cJSON *obj) {
	cJSON* carPositions = NULL;
	cJSON* data = NULL;
	cJSON* array = NULL;
	cJSON* id = NULL;
	cJSON* name = NULL;
	cJSON* color = NULL;
	cJSON* angle = NULL;
	cJSON* piecePosition = NULL;
	cJSON* pieceIndex = NULL;
	cJSON* inPieceDistance = NULL;
	cJSON* lane = NULL;
	cJSON* startLaneIndex = NULL;
	cJSON* endLaneIndex = NULL;
	cJSON* lap = NULL;
	cJSON* gameid = NULL;
	cJSON* gameTick = NULL;
	//cJSON* ptr1 = NULL;
	//cJSON* ptr2 = NULL; 
	int i, index;

	carPositions = obj->child;
	data = carPositions->next;
	array = data->child;
	
	while (array != NULL) {
	id = array->child;
	name = id->child;
	index = 0xFF;
	
	for (i = 0; i < num_of_cars; i ++) {
		//printf(" %s %s \n", name->valuestring, car_table[i].name);
		if (!strcmp(name->valuestring,car_table[i].name)) {
			index = i;
			//printf("Found: index = %d\n", i);
		}
	}

	color = name->next;
	angle = id->next;
	piecePosition = angle->next;
	pieceIndex = piecePosition->child;
	inPieceDistance = pieceIndex->next;
	lane = inPieceDistance->next;
	startLaneIndex = lane->child;
	endLaneIndex = startLaneIndex->next;
	lap = lane->next;
		
	if (index > num_of_cars -1 ) {
		/* safety check */
	}
	else {
		car_table[index].old_angle = car_table[index].angle;
		car_table[index].old_pieceIndex = car_table[index].pieceIndex;		
		car_table[index].old_inPieceDistance = car_table[index].inPieceDistance;
		car_table[index].old_startLaneIndex = car_table[index].startLaneIndex;
		car_table[index].old_endLaneIndex = car_table[index].endLaneIndex;
		car_table[index].old_lap = car_table[index].lap;

		car_table[index].old_speed = car_table[index].speed;			
		car_table[index].old_acceleration = car_table[index].acceleration;			


		car_table[index].angle = angle->valuedouble;
		car_table[index].pieceIndex = pieceIndex->valueint;
		car_table[index].inPieceDistance = inPieceDistance->valuedouble;
		car_table[index].startLaneIndex = startLaneIndex->valueint;
		car_table[index].endLaneIndex = endLaneIndex->valueint;
		car_table[index].lap = lap->valueint;

		car_table[index].speed = calculateCarSpeed(index);
		if (abs(car_table[index].speed - car_table[index].old_speed) > 1.0) {
			/* error in speed calculation ?*/
		}
		car_table[index].acceleration = car_table[index].speed - car_table[index].old_speed;			


	}


	array = array->next;
	} // while
	gameid = data->next; // gameid	
	gameTick = gameid->next;
	if (gameTick != NULL)
		g_gameTick = gameTick->valueint;
}

static void handleYourCar(cJSON *obj) {
	cJSON* yourCar = NULL;
	cJSON* data = NULL;
	cJSON* name = NULL;
	cJSON* color = NULL;
	cJSON* gameid = NULL;
	//cJSON* ptr1 = NULL;
	//cJSON* ptr2 = NULL;
	yourCar = obj->child;
	data = yourCar->next;
	name = data->child;
	sprintf(myName,"%s",name->valuestring);
	color = name->next;
	sprintf(myColor,"%s",name->valuestring);
	gameid = data->next;
}



static void handleGameStart(cJSON *obj) {
	cJSON* gameStart = NULL;
	cJSON* data = NULL;
	gameStart = obj->child;
	data = gameStart->next;
}

int measurement_done = 0;

#define MEASUREMENT_START 3
#define MEASUREMENT_LENGTH 10
#define MEASUREMENT_EXTRA 4
float measurement_velocity[MEASUREMENT_START + MEASUREMENT_LENGTH + MEASUREMENT_EXTRA];
float measurement_acceleration[MEASUREMENT_START + MEASUREMENT_LENGTH + MEASUREMENT_EXTRA];

float measurement_sum1, measurement_sum2, measurement_sum3, measurement_sum4;

float acceleration_at_startup;
float acceleration_speed_dependency;

void make_measurement(int gameTick) {
	float numerator_1, denominator_1, numerator_2, denominator_2;
	int i;

	if (gameTick < MEASUREMENT_START)  {
		measurement_sum1 = 0;
		measurement_sum2 = 0;
		measurement_sum3 = 0;
		measurement_sum4 = 0;
	}

	if ((gameTick >= MEASUREMENT_START) && (gameTick < MEASUREMENT_START + MEASUREMENT_LENGTH)) {
		if ((car_table[my_car_index].speed == 0) || (car_table[my_car_index].speed < 0)) {
			printf("warning: speed value in calibration\n");
		}
		if ((car_table[my_car_index].acceleration == 0) || (car_table[my_car_index].acceleration < 0)) {
			printf("warning: acceleration value in calibration\n");
		}
		measurement_velocity[gameTick] = car_table[my_car_index].speed;
		measurement_acceleration[gameTick] = car_table[my_car_index].acceleration; 

		measurement_sum1 += measurement_velocity[gameTick]* measurement_acceleration[gameTick]; 
		measurement_sum2 += measurement_velocity[gameTick];
		measurement_sum3 += measurement_acceleration[gameTick];
		measurement_sum4 += measurement_velocity[gameTick]*measurement_velocity[gameTick];
	}
	if (gameTick == MEASUREMENT_START + MEASUREMENT_LENGTH) {

		numerator_1 = ((float)MEASUREMENT_LENGTH) * measurement_sum1 - measurement_sum2 * measurement_sum3;
		denominator_1 = ((float)MEASUREMENT_LENGTH) * measurement_sum4 - measurement_sum2 * measurement_sum2;
		if (denominator_1 != 0) {
			acceleration_speed_dependency = numerator_1 / denominator_1;
		}
		numerator_2 = measurement_sum3 -acceleration_speed_dependency * measurement_sum2;
		denominator_2 = (float)MEASUREMENT_LENGTH;
		if (denominator_2 != 0) {
			acceleration_at_startup = numerator_2 / denominator_2;
		}		
		printf("acceleration_speed_dependency %f\n", acceleration_speed_dependency);
		printf("acceleration_at_startup %f\n", acceleration_at_startup);	
#if 0
		fprintf(datafile, "acceleration_speed_dependency %f\n", acceleration_speed_dependency);
		fprintf(datafile, "acceleration_at_startup %f\n", acceleration_at_startup);	
#endif
		if ((acceleration_speed_dependency > 0) || (acceleration_speed_dependency < -0.1)){
			printf("fault in calibration \n");
			acceleration_speed_dependency = -0.02;		
		}
		measurement_done = 1;
	}
	if (gameTick > MEASUREMENT_START + MEASUREMENT_LENGTH) {
		if (!measurement_done) {
			printf("fault in calibration \n");
			acceleration_speed_dependency = -0.02;		
			measurement_done = 1;
		}
	}

}

float find_bend_angle() {
	int i = 0;
	int ESTIMATE_LENGTH = 5;
	float angle = 0;
	for (i = 0; i < ESTIMATE_LENGTH; i++)
		angle+= piece_table[(car_table[my_car_index].pieceIndex +i) % num_of_pieces].angle;
	return angle;
}

float find_abs_bend_angle() {
	int i = 0;
	int ESTIMATE_LENGTH = 5;
	float angle = 0;
	for (i = 0; i < ESTIMATE_LENGTH; i++)
		angle+= abs(piece_table[(car_table[my_car_index].pieceIndex +i) % num_of_pieces].angle);
	return angle;
}

float find_bend_radius() {
	int i = 0;
	int ESTIMATE_LENGTH = 5;
	float  tmp = 0;
	float radius = 200.0;
	for (i = 0; i < ESTIMATE_LENGTH; i++) {
		tmp = piece_table[(car_table[my_car_index].pieceIndex +i) % num_of_pieces].radius;
		if (tmp == 0)
			tmp = 200.0;
		if (radius > tmp) 
			radius = tmp;
	}
	return radius;
}

float find_speed_limit_for_index(int i) {
	float speed_limit = 20.0;	
	// 0.36 keimola 0.30 germany/french
	float length = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].length;
	int is_length = piece_table[(car_table[my_car_index].pieceIndex +i) % num_of_pieces].is_length;	
	float angle = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].angle;
	int is_angle = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].is_angle;
	float radius = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].radius;
	int is_radius = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].is_radius;
	
	float dfc = lane_table[car_table[my_car_index].endLaneIndex].distanceFromCenter;

	if (is_length) { /* straight */				
		speed_limit = 15.0;
	}
	else if (((is_radius) && (is_angle)) && (angle > 0)) { /* bend to right */
		speed_limit = sqrt(speed_tuning*(radius - dfc));
	}
	else if (((is_radius) && (is_angle)) && (angle < 0)) { /* bend to left */
		speed_limit = sqrt(speed_tuning*(radius  + dfc)); 
	}

	return speed_limit;
}


float calculate_time_to_piece(float distance, float speed) {
	if (speed != 0)
		return distance/speed;
	else
		return 10000.0;
}

/* num = 0 -> distance to end of this piece */
/* num = 1 -> distance to end of next piece */
/* and so on */
float calculate_distance_to_piece(int num) {
	float length, angle, radius;
	int is_length, is_angle, is_radius;
	float dfc, inPieceDistance;
	float distance;
	int i;	

	length = piece_table[car_table[my_car_index].pieceIndex].length;
	is_length = piece_table[car_table[my_car_index].pieceIndex].is_length;
	angle = piece_table[car_table[my_car_index].pieceIndex].angle;
	is_angle = piece_table[car_table[my_car_index].pieceIndex].is_angle;
	radius = piece_table[car_table[my_car_index].pieceIndex].radius;
	is_radius = piece_table[car_table[my_car_index].pieceIndex].is_radius;

	dfc = lane_table[car_table[my_car_index].startLaneIndex].distanceFromCenter;

	inPieceDistance = car_table[my_car_index].inPieceDistance; 
	distance = 0.0;

	if (is_length) { /* straight */				
		distance = length;
	}
	else if (((is_radius) && (is_angle)) && (angle > 0)) { /* bend to right */
		distance = (angle * 3.14 * (radius - dfc)) / 180.0; 
	}
	else if (((is_radius) && (is_angle)) && (angle < 0)) { /* bend to left */
		distance = ((-angle) * 3.14 * (radius  + dfc)) / 180.0; 
	}
	
	distance-= inPieceDistance;
	if (distance < 0)
		distance = 0;
	if (num == 0) 
		return distance;

	for (i = 1; i < num + 1; i++) {

		length = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].length;
		is_length = piece_table[(car_table[my_car_index].pieceIndex +i) % num_of_pieces].is_length;	
		angle = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].angle;
		is_angle = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].is_angle;
		radius = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].radius;
		is_radius = piece_table[(car_table[my_car_index].pieceIndex + i) % num_of_pieces].is_radius;

		if (is_length) { /* straight */				
			distance += length;
		}
		else if (((is_radius) && (is_angle)) && (angle > 0)) { /* bend to right */
			distance += (angle * 3.14 * (radius - dfc)) / 180.0; 
		}
		else if (((is_radius) && (is_angle)) && (angle < 0)) { /* bend to left */
			distance += ((-angle) * 3.14 * (radius  + dfc)) / 180.0; 
		}
	}
	return distance;
}

float find_speed_limit(float pangle, float pradius) {
	float speed_limit = 20.0;	
	int i = 0;
	float tmp = 0;
	float speed_limit_piece_i;
	float distance_to_piece_i;
	float time_to_piece_i;
	float speed_now;
	float brake_acceleration;
	float realistic_speed_reduction;
	for (i = 0; i < 5 /* 5 */; i++) {

		speed_limit_piece_i = find_speed_limit_for_index(i);
		distance_to_piece_i = calculate_distance_to_piece(i);
		speed_now = car_table[my_car_index].speed;
		time_to_piece_i = calculate_time_to_piece(distance_to_piece_i, speed_now);
		brake_acceleration =  (speed_now + speed_limit_piece_i) * 0.5 * acceleration_speed_dependency;
		realistic_speed_reduction = brake_acceleration*time_to_piece_i;
		//printf("realistic speed red %d %f\n", i, realistic_speed_reduction);
		if (speed_limit > speed_limit_piece_i - realistic_speed_reduction)
			speed_limit = speed_limit_piece_i - realistic_speed_reduction;		
	}

	return speed_limit;
}


float make_throttle_decision() {
	
	float t = 1.0;

	float bend_angle = find_bend_angle();
	float abs_bend_angle = find_abs_bend_angle();
	float radius = find_bend_radius();
	float speed_limit = find_speed_limit(abs_bend_angle, radius);
	float current_speed = car_table[my_car_index].speed;
	float acceleration = car_table[my_car_index].speed - car_table[my_car_index].old_speed;;
	int current_piece = car_table[my_car_index].pieceIndex;
	float current_bend =  piece_table[(car_table[my_car_index].pieceIndex)].angle;
	float current_radius =  piece_table[(car_table[my_car_index].pieceIndex)].radius;
	float car_angle = car_table[my_car_index].angle;
	
	if (!measurement_done) {
		make_measurement(g_gameTick);
	}
#if 0
//small advantage ?
	if (abs(car_angle) < 1.0) {
		speed_limit+=1.0;
	}
	if (abs(car_angle) > 20.0) {
		speed_limit-=1.0;
	}

#endif

	if (current_speed < (speed_limit )) {
		t = 1.0;
	}
	else {
		t = 0.0;
	}
	float distance_to_next_piece = calculate_distance_to_piece(0);
	float brake_acceleration =  current_speed * acceleration_speed_dependency;	
	float braking_time_to_next_piece =calculate_time_to_piece(distance_to_next_piece, current_speed);

#if 0
	printf("distance %f, time %f, brake %f, speed %f acc %f limit %f t %f tick %d index %d  ba %f br %f ca %f abend %f \n ",
			distance_to_next_piece, 
			braking_time_to_next_piece,
			brake_acceleration,
			current_speed,
			acceleration,
			speed_limit, 
			t,
			g_gameTick,
			current_piece,	
			current_bend,
			current_radius,
			car_angle,
			abs_bend_angle
			);
#endif
#if 0
	fprintf(datafile, "distance %f, time %f, brake %f, speed %f acc %f limit %f t %f tick %d index %d  ba %f br %f ca %f abend %f \n ", 
			distance_to_next_piece, 
			braking_time_to_next_piece,
			brake_acceleration,
			current_speed,
			acceleration,
			speed_limit, 
			t,
			g_gameTick,
			current_piece,	
			current_bend,
			current_radius,
			car_angle,
			abs_bend_angle
			);
	int a = g_gameTick;
	int b = (int) 1000*current_speed;
	int c = (int) 1000*acceleration;	
	if ((t > 0) && (acceleration > 0)) {
		fwrite(&a, sizeof(int), 1, accelerationFile);
		fwrite(&b, sizeof(int), 1, accelerationFile);
		fwrite(&c, sizeof(int), 1, accelerationFile);
	}
	if ((t == 0) && (acceleration < 0)) {
		fwrite(&a, sizeof(int), 1, brakeFile);
		fwrite(&b, sizeof(int), 1, brakeFile);
		fwrite(&c, sizeof(int), 1, brakeFile);
	}
#endif

#if 0
	/* test code */
	if (g_gameTick > 20) {
	if (is_anybody_on_left_lane()) {
		printf("someone on left\n");
	}

	if (is_anybody_on_right_lane()) {
		printf("someone on right\n");
	}
	if (is_anybody_on_same_lane()) {
		printf("someone on same lanet\n");
	}
#endif

#if 0
	/* test code */
	if (! (is_anybody_on_left_lane() || is_anybody_on_right_lane()) )
		t=0;
#endif

	/* make sure to ... */
	if (current_speed < 3.0) {
		t = 1.0;
	}
	return  t;
}


float find_next_bend() {
	float next_bend = 0;
	int i = 0;
	while ((next_bend == 0) && (i < 50)) {
		float piece_angle = piece_table[(car_table[my_car_index].pieceIndex +i + 1) % num_of_pieces].angle;
		if (piece_angle != 0)
			next_bend = piece_angle;
		i++;
		}
	return next_bend;
}


float find_bend_between_switches() {
	float bend = 0;
	int i = 0;
	float angle = 0;
	int is_switch = 0;
	int switches_found = 0;
	while ((switches_found < 2) && (i < 20)) {
		is_switch = piece_table[(car_table[my_car_index].pieceIndex +i + 1) % num_of_pieces].is_switch;
		angle = piece_table[(car_table[my_car_index].pieceIndex +i + 1) % num_of_pieces].angle;
		if (is_switch)
			switches_found++;
		if (switches_found > 0)
			bend+= angle;
		i++;
		}
	return bend;
}



int left_lane_exists() {
	int myLaneIndex = car_table[my_car_index].startLaneIndex;
	float myLaneDistance = lane_table[myLaneIndex].distanceFromCenter;
	float lda = 0;
	float ldb = 0;
	if (myLaneIndex == 0) {
		ldb = lane_table[myLaneIndex +1].distanceFromCenter;
		if (ldb < myLaneDistance) 
			return 1;
	} 
	else if (myLaneIndex == num_of_lanes -1) {
		lda = lane_table[myLaneIndex -1].distanceFromCenter;
		if (lda < myLaneDistance)
			return 1;
	}
	else {
		lda = lane_table[myLaneIndex -1].distanceFromCenter;
		ldb = lane_table[myLaneIndex +1].distanceFromCenter;
		if (lda < myLaneDistance)
			return 1;
		if (ldb < myLaneDistance)
			return 1;
	}
	return 0;
}

int right_lane_exists() {
	int myLaneIndex = car_table[my_car_index].startLaneIndex;
	int myLaneDistance = lane_table[myLaneIndex].distanceFromCenter;
	float lda = 0;
	float ldb = 0;
	if (myLaneIndex == 0) {
		ldb = lane_table[myLaneIndex +1].distanceFromCenter;
		if (ldb > myLaneDistance) 
			return 1;
	} 
	else if (myLaneIndex == num_of_lanes -1 ) {
		lda = lane_table[myLaneIndex -1].distanceFromCenter;
		if (lda > myLaneDistance)
			return 1;
	}
	else {
		lda = lane_table[myLaneIndex -1].distanceFromCenter;
		ldb = lane_table[myLaneIndex +1].distanceFromCenter;
		if (lda > myLaneDistance)
			return 1;
		if (ldb > myLaneDistance)
			return 1;
	}
	return 0;
}

int decide_to_switch_left() {
	int result = 1;
	float next_bend = find_bend_between_switches(); //find_next_bend();
	int leftLaneExists = left_lane_exists();
	int switchComing = piece_table[car_table[my_car_index].pieceIndex +1 ].is_switch;

#if 0
	if (next_bend < 0) {
		printf("bend to left ");
	}
	if (next_bend > 0) {
		printf("bend to right ");
	}

	if (!leftLaneExists) {
		printf("no left lane ");
	}
	else {
		printf("is left lane ");
	}
#endif

	if (!switchComing) {
		result = 0;
	}
	if (!leftLaneExists) {
		result = 0;
	}
	if (next_bend >= 0) {
		result = 0;
	}
	if (is_anybody_on_left_lane()) {
		result = 0;
	}
	/* result = 0;  *//* test code to disable left lane switch */
	return result;
}

int decide_to_switch_right() {
	int result = 1;
	float next_bend = find_bend_between_switches(); //find_next_bend();
	int rightLaneExists = right_lane_exists();
	int switchComing = piece_table[car_table[my_car_index].pieceIndex +1 ].is_switch;
#if 0
	if (next_bend < 0) {
		printf("bend to left ");
	}
	if (next_bend > 0) {
		printf("bend to right ");
	}

	if (!rightLaneExists) {
		printf("no right lane ");
	}
	else {
		printf("is right lane ");
	}
#endif
	if (!switchComing) {
		result = 0;
	}
	if (!rightLaneExists) {
		result = 0;
	}
	if (next_bend <= 0) {
		result = 0;
	}
	if (is_anybody_on_right_lane()) {
		result = 0;
	}
	/* result = 0; */ /* test code to disable right lane switch */
	return result;
}

struct turbo_data {
	float turboDurationMilliSeconds;
	int turboDurationTicks;
	float turboFactor;
};

struct turbo_data turboStatus;

static void handleTurboAvailable(cJSON *obj) {
	cJSON* turboAvailable = NULL;
	cJSON* data = NULL;
	cJSON* turboDurationMilliSeconds = NULL;
	cJSON* turboDurationTicks = NULL;
	cJSON* turboFactor= NULL;
	//cJSON* ptr1 = NULL;
	//cJSON* ptr2 = NULL;
	turboAvailable = obj->child;
	data = turboAvailable->next;
	turboDurationMilliSeconds = data->child;
	turboStatus.turboDurationMilliSeconds = turboDurationMilliSeconds->valuedouble;
	turboDurationTicks = turboDurationMilliSeconds->next;
	turboStatus.turboDurationTicks = turboDurationTicks->valueint;
	turboFactor = turboDurationTicks->next;
	turboStatus.turboFactor = turboFactor->valuedouble;

	//printf("turbo: %f %d %f \n", turboStatus.turboDurationMilliSeconds, 
	//	turboStatus.turboDurationTicks, turboStatus.turboFactor);
}

float distance_between_cars(int index) {
	float distance = 0;
	
	if (car_table[my_car_index].pieceIndex == car_table[index].pieceIndex) {
		distance = car_table[my_car_index].inPieceDistance - car_table[index].inPieceDistance;
	}
	else if (car_table[my_car_index].old_pieceIndex == car_table[index].old_pieceIndex) {
		distance = car_table[my_car_index].old_inPieceDistance - car_table[index].old_inPieceDistance;
	}
	else if (car_table[my_car_index].pieceIndex < car_table[index].pieceIndex) {
		distance = -100.0;
	}
	else if (car_table[my_car_index].pieceIndex > car_table[index].pieceIndex) {
		distance = +100.0;
	}
	else {
		distance = 1000;
	}


	return distance; 

}

#define LEFT_LANE 1
#define SAME_LANE 2
#define RIGHT_LANE 3

int is_anybody_next_to_me(int searched_direction) {

	int index = 0;
	float distance  = 0;
	float myDfc = 0;
	float yourDfc = 0;
	int result = 0;
	for (index = 0; index < num_of_cars; index++) {
		if (index == my_car_index) {
			/* ignore my car */
		}
		else if (((car_table[index].pieceIndex != car_table[my_car_index].pieceIndex) &&
			(car_table[index].pieceIndex != car_table[my_car_index].pieceIndex - 1)) &&
			(car_table[index].pieceIndex != car_table[my_car_index].pieceIndex + 1))
			{
			/* ignore cars far away */
		}
		else  {
			distance  = distance_between_cars(index);
			//printf("distance = %f ", distance);
			if (abs(distance)  < 40.0) {
				myDfc = lane_table[car_table[my_car_index].startLaneIndex].distanceFromCenter;
				yourDfc = lane_table[car_table[index].startLaneIndex].distanceFromCenter;
				if (myDfc > yourDfc) {
					/* you are on left lane */
					if (searched_direction == LEFT_LANE)
						result = 1;
				}
				if (myDfc < yourDfc) {
					/* you are on right lane */
					if (searched_direction == RIGHT_LANE)
						result = 1;
				}
				if (myDfc == yourDfc) {
					/* you are on same lane */
					if (searched_direction == SAME_LANE)
						result = 1;
				}		

			}

		}
	}
	return result;
}

int is_anybody_on_left_lane() {
	return is_anybody_next_to_me(LEFT_LANE);
}

int is_anybody_on_right_lane() {
	return is_anybody_next_to_me(RIGHT_LANE);
}

int is_anybody_on_same_lane() {
	return is_anybody_next_to_me(SAME_LANE);
}

static void handleCrash(cJSON *obj) {
	cJSON* crash = NULL;
	cJSON* data = NULL;
	cJSON* name = NULL;
	cJSON* color = NULL;
	cJSON* gameid = NULL;
	//cJSON* ptr1 = NULL;
	//cJSON* ptr2 = NULL;
	crash = obj->child;
	data = crash->next;
	name = data->child;
	//debug_json(name);
	//printf("myName %s\n",name->valuestring);
	if (is_qualifying) { 
		if (!strcmp(name->valuestring, myName)) {
			puts("oops let's drive bit slower");
			speed_tuning-=0.02;
			printf("speed_tuning = %f\n", speed_tuning);
		}
	}
	else {
		if (!strcmp(name->valuestring, myName)) {
			puts("oh no it is me");
			speed_tuning-=0.02;
			printf("speed_tuning = %f\n", speed_tuning);
		}
	
	}
	color = name->next;
	//printf("myColor %s\n",color->valuestring);
	gameid = data->next;
	//printf("gameid %s\n", gameid->valuestring);
}

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

int main(int argc, char *argv[])
{
    int sock;
    cJSON *json;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

     measurement_done = 0;

#if 0
    datafile = fopen("data.txt","a");
    accelerationFile = fopen("accdata.txt","wb");
    brakeFile = fopen("brakedata.txt","wb");
#endif
    sock = connect_to(argv[1], argv[2]);
#if 1
    json = join_msg(argv[3], argv[4]);
#endif
#if 0
    /* alternative */ 
    json = create_race_msg(argv[3], argv[4]); 
#endif
    write_msg(sock, json);
    cJSON_Delete(json);
    puts("Running main...");
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;


     	if (!strcmp("carPositions", msg_type_name)) {
		handleCarPositions(json);
		//printCarPositions();
        }
	else if (!strcmp("gameInit", msg_type_name)) {
		puts("Received gameInit");
		handleGameInit(json);
		printGameInit();
	}
	else if (!strcmp("yourCar", msg_type_name)) {
		puts("Received yourCar");
		handleYourCar(json);
	}
	else if (!strcmp("gameStart", msg_type_name)) {
		puts("Received gameStart");
		handleGameStart(json);
	}
	else if (!strcmp("turboAvailable", msg_type_name)) {
		handleTurboAvailable(json);
	}
	else if (!strcmp("gameEnd", msg_type_name)) {
		puts("Received gameEnd");
	}
	else if (!strcmp("tournamentEnd", msg_type_name)) {
		puts("Received tournamentEnd");
	}
	else if (!strcmp("crash", msg_type_name)) {
		puts("Received crash");
		handleCrash(json);
	}
        else {
            log_message(msg_type_name, json);
	}

	if (!strcmp("carPositions", msg_type_name)) {
		if (car_table[my_car_index].pieceIndex != car_table[my_car_index].old_pieceIndex) {
		//printf("new piece %d ", car_table[my_car_index].pieceIndex);
			if (decide_to_switch_left()) {	
				msg = switch_left_msg();
			}
			else if (decide_to_switch_right()) {
				msg = switch_right_msg();
			}
			else {
				msg = throttle_msg((double)make_throttle_decision());
			}
		} else if ((turboStatus.turboDurationTicks != 0.0) && (find_abs_bend_angle() == 0.0)){
			turboStatus.turboDurationTicks = 0.0;
			msg = turbo_msg();
		}
		else {
			msg = throttle_msg((double)make_throttle_decision());    
			//msg = throttle_msg(0.5); /* 0.5 */
		}
       
	}
	else {
       		msg = ping_msg();
        }

#if 0
        if (!strcmp("carPositions", msg_type_name)) {
            msg = throttle_msg(0.5);
        } else {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }
#endif
        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }

#if 0
    fclose(datafile);
    fclose(accelerationFile);
    fclose(brakeFile);
#endif
    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *create_race_msg(char *bot_name, char *bot_key)
{
    cJSON *data1 = cJSON_CreateObject();
    cJSON_AddStringToObject(data1, "name", bot_name);
    cJSON_AddStringToObject(data1, "key", bot_key);

    cJSON *data2 = cJSON_CreateObject();
    cJSON_AddItemToObject(data2, "botId", data1);
    cJSON_AddStringToObject(data2, "trackName", "suzuka" /*"england"*/ /*"elaeintarha"*/  /* "france"*/  /* "usa" */ /*"keimola"*/  /* "germany" */);
    cJSON_AddStringToObject(data2, "password", "abc");
    cJSON_AddNumberToObject(data2, "carCount", 1);
    return make_msg("createRace" /*"createRace" */, data2);
}

static cJSON *switch_left_msg()
{
    return make_msg("switchLane", cJSON_CreateString("Left"));
}

static cJSON *switch_right_msg()
{
    return make_msg("switchLane", cJSON_CreateString("Right"));
}

static cJSON *turbo_msg()
{
    return make_msg("turbo", cJSON_CreateString("go go go"));
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
